window.onload = function(){
//Task 1
let name = document.querySelector('#name');
let btn = document.querySelector('#save');
let btnShow = document.querySelector('#show');
let text = document.querySelector('p');
btn.addEventListener('click', function(){
    window.sessionStorage.userName = name.value;
    name.value = '';
});
btnShow.addEventListener('click', function(){
    text.innerText = window.sessionStorage.getItem("userName");
});

//Task 2
let body = document.querySelector('body');
let typeColor = document.querySelector('#color');
typeColor.addEventListener('change', function(){
    body.style.backgroundColor = this.value;
    window.localStorage.Background = this.value;
    
});
body.style.backgroundColor = window.localStorage.getItem("Background");

//Task 3??
let formList = document.querySelector('.toDoList');
let addBtn = document.querySelector('#addbtn');
let toDo = document.querySelector('#task');
let newList = document.querySelector('.list');
let arr = [];
let parseList = JSON.parse(window.localStorage.getItem('task'));
parseList.forEach(function(item) {
    let newLi = document.createElement('li');
    let removeBtn = document.createElement('button');
    removeBtn.innerHTML = 'Remove';

    newList.appendChild(newLi);
    newLi.innerHTML = item;
    newLi.appendChild(removeBtn);

    removeBtn.addEventListener('click', function(){
        newLi.remove();
        arr.pop();
    });  
  });

addBtn.addEventListener('click', function(e){
    e.preventDefault();
    if(toDo.value === ''){
        alert('Enter a text')}
        else {
    
    let newLi = document.createElement('li');
    let removeBtn = document.createElement('button');
    removeBtn.innerHTML = 'Remove';

    newList.appendChild(newLi);
    newLi.innerHTML = toDo.value;
    newLi.appendChild(removeBtn);
    arr.push(toDo.value);
    
    formList.reset();
    removeBtn.addEventListener('click', function(){
        newLi.remove();  
        arr.pop();
    });  
    localStorage.setItem('task', JSON.stringify(arr));  
}
})

//Task 4
let user = { 
    name: "Oksana",
    age: 37,
    kids: true, 
    address:{
        Country: "Ukraine",
        City: "Kyiv"
    },
    skills: ["html", "css", "javascript"]    
  };
  window.localStorage.userData = JSON.stringify(user);
    let parseData = JSON.parse(window.localStorage.getItem("userData"));
    console.log(parseData);
    //window.localStorage.clear();
}