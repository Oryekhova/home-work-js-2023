console.log("----Push----");
//Push
//1
let newArr = [];
newArr.push("Oksana", "Oryekhova", 1986);
console.log(newArr);
//2
let arrayPush = ["Hello", 12, true];
let pushArr = (arr, element)=>arr.push(element);
pushArr(arrayPush, "Bye");
console.log(arrayPush);

console.log("----Pop----");
//Pop
//1
let arrPop = [1, 2, 3, 4, 5];
arrPop.pop(-1);
console.log(arrPop);
//2
let arrayPop = [1, 2, 3];
let popArr = (arr, element)=>arr.pop(element);
popArr(arrayPop, -1);
console.log(arrayPop);

console.log("----Unshift----");
//Unshift
//1
let arrUnshift = [1, 2, 3, 4, 5];
arrUnshift.unshift("Element");
console.log(arrUnshift);
//2
let arrayUnshift = [1, 2, 3];
let unshiftArr = (arr, element)=>arr.unshift(element);
unshiftArr(arrayUnshift, "Element");
console.log(arrayUnshift);

console.log("----Shift----");
//Shift
//1
let arrShift = [1, 2, 3, 4, 5];
arrShift.shift();
console.log(arrShift);
//2
let arrayShift = [1, 2, 3];
let shiftArr = (arr)=>arr.shift();
shiftArr(arrayShift);
console.log(arrayShift);

console.log("----Fill----");
//Fill
//1
let arrFill = new Array(10);
arrFill.fill(5);
console.log(arrFill);

//2
let arrayFill = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let fillArr = (arr, element, index)=>arr.fill(element, index);
fillArr(arrayFill, "§", 6);
fillArr(arrayFill, "*", 8);
console.log(arrayFill);

console.log("----Splice----");
//Splice
//1
let arrSplice = [1, 2, 3, 4, 5, 6, 7, 8];
arrSplice.splice(2, 3);
console.log(arrSplice);
//2
let arraySplice = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let spliceArr = (arr, index, quant)=>arr.splice(index, quant);
spliceArr(arraySplice, 3, 4);
console.log(arraySplice);

console.log("----Reverse----");
//Reverse
//1
let arrReverse = [1, 2, 3, 4, 5];
arrReverse.reverse();
console.log(arrReverse);
//2
let arrayReverse = [1, 2, 3, 4, 5];
let reverseArr = (arr)=>arr.reverse();
reverseArr(arrayReverse);
console.log(arrayReverse);

console.log("----Concat----");
//Concat
//1
let arr1 = [1, 2, 3];
let arr2 = ["a", "b", "c"];
arr1 = arr1.concat(arr2);
console.log(arr1);
//2
let arr1Concat = [1, 2, 3];
let arr2Concat = ["a", "b", "c"];
let concatArr = (ar1, ar2)=>arr3=ar1.concat(ar2);
concatArr(arr1Concat, arr2Concat);
console.log(arr3);

console.log("----Includes----");
//Includes
//1
let arrIncludes = [1, 2, "a", "b"];
console.log(arrIncludes.includes("a"));
//2
let arrayIncludes = [1, 2, "a", "b"];
let includesArr = (arr, element)=>arr.includes(element);
includesArr(arrayIncludes, "b");
console.log(includesArr(arrayIncludes, "b"));

console.log("----Filter----");
//Filter
//1
let arrFilter = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
function evenNumbers(value){
    return value%2 === 0;
}
let filterResult = arrFilter.filter(evenNumbers);
console.log(filterResult);
//2
let arrayFilter = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
function condition (value){
    return value%2 === 0;
}
let filterArr = (arr, func)=>newFilter=arr.filter(func);
filterArr(arrayFilter, condition);
console.log(newFilter);

console.log("----Map----");
//Map
//1
let arrMap = [2, 3, 4];
let newMapArr = arrMap.map((value)=>value**2);
console.log(newMapArr);
//2
let arrayMap= [1, 2, 3, 4, 5];
function transform (value){
    return value=String(value);
}
let mapArr = (arr, func)=>newMap=arr.map(func);
mapArr(arrayMap, transform);
console.log(newMap);