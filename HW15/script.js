window.onload = function(){
//Task 1
let button = document.querySelector('.btn');
let text = document.querySelector('p');
button.addEventListener('click', function(){
    text.style.color = 'red';
});

//Task 2
let square = document.querySelector('.block');
square.addEventListener('dblclick', function(){
    let width = square.offsetWidth*2;
    let height = square.offsetHeight*2;
    square.style.width = width + 'px';
    square.style.height = height + 'px';
console.log(width, height);
})

//Task 3
let btn = document.querySelector('.counter');
let count = document.querySelector('.number');
plusOne=()=>count.innerHTML= ++count.innerHTML;
btn.addEventListener('click', plusOne);
btn.addEventListener('click', function(){
    if (count.innerHTML>=10){
        btn.removeEventListener('click', plusOne);
    }
});

//Task 4
//Version 1:
let element = document.querySelectorAll('.list');
for (let el of element){
    el.addEventListener('click', function(){
    el.remove();
})};
//Version 2 "This":
/*let [...divElements] = document.querySelectorAll('.list');
divElements.forEach(item=>{
    item.onclick = function(){
        this.remove();
    }
})*/

//Task5
let container = document.querySelector('.blockcontainer');
container.onclick = function(e){
  alert(`You choosed button "${e.target.innerHTML}"`);
    }
}
