/*
//Game
let value = prompt("Enter a value: scissors, stone or paper");
let lowerCase = value.toLowerCase();
let arrValue = ["scissors", "stone", "paper"];
let indexOfVal = Math.floor(Math.random()*3);
function randomValue () {
    if (indexOfVal === 0) {
        indexOfVal=arrValue[0]
    } else if (indexOfVal === 1) {
        indexOfVal=arrValue[1]
    } if (indexOfVal === 2) {
        indexOfVal=arrValue[2]
    } 
};
randomValue();
function Result () {
    if (lowerCase == indexOfVal) {
        alert("It's a draw!")
    } else if (lowerCase == "scissors" && indexOfVal === "paper" || lowerCase == "stone" && indexOfVal === "scissors"|| lowerCase == "paper" && indexOfVal === "stone") {
        alert("Congratulations! You won!")
    } else if (lowerCase == "paper" && indexOfVal === "scissors" || lowerCase == "scissors" && indexOfVal === "stone"|| lowerCase == "stone" && indexOfVal === "paper") {
        alert("You lose!")
    } else {
        alert("You entered wrong value!")
    }
}
Result();
console.log(`Your value: ${lowerCase}`);
console.log(`Computer's value: ${indexOfVal}`);
*/
//Math.min & Math.max
console.log("==Math.min & Math.max==");
let minNumber = (a, b) => console.log(`The smallest number is: ${Math.min(a, b)}`);
minNumber(1, 8);

let maxNumber = (a, b) => console.log(`The biggest number is: ${Math.max(a, b)}`);
maxNumber(1, 8);

//Math.pow
console.log("==Math.pow==");
let squarePow = (a, b) => console.log(Math.pow(a, b));
squarePow(7, 2);

//Math.floor & Math.ceil
console.log("==Math.floor & Math.ceil==");
let mathFloor = (a) => console.log(Math.floor(a*10));
mathFloor(Math.random());

let mathCeil = (a) => console.log(Math.ceil(a*10));
mathCeil(Math.random());

//Date.getYear
console.log("==Date.getYear==");
let usersAge = Number(prompt("Enter your age:"));
let nowDate = new Date();
let usersYear = () => console.log(nowDate.getFullYear()-usersAge);
usersYear();

//Date.toLocaleString
console.log("==Date.toLocaleString==");
let today = new Date;
console.log(today.toLocaleString());

//String.split & toUpperCase
console.log("==String.split & toUpperCase==");
let cities = 'London Kyiv Paris Berlin';
let toUpperCase = cities.toUpperCase();
let citiesArr = toUpperCase.split(' ');
console.log(citiesArr);


