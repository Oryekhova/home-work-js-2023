//Spread
console.log("--Spread--");
let firstArr = [1, 2, 3];
let secondArr = ["a", "b", "c"];
let spreadArr = (spr1, spr2) => thirdArr=[...spr1, ...spr2];
spreadArr(firstArr, secondArr);
console.log(`New Array is:`, thirdArr);

//Rest
console.log("--Rest--");
function restFunc(str, ...arg){
    console.log(str, arg);
}
restFunc("Hello", 3, 5 ,6, 7);

function average(...theArgs) {
    let result = 0;
    for (const arg of theArgs) {
      result += arg/theArgs.length;
    }
    return result;
  }
  
  console.log(average(3, 5, 7));

//TypeOf
console.log("--TypeOf--");
let dataType = (val) => console.log(`Data type of "${val}" is ${typeof val}`);
dataType("Hello!");

function checkType(data){
    if (typeof data === 'number'){
        console.log(`Type of value "${data}" is number`)
    } else if (typeof data === 'string'){
        console.log(`Type of value "${data}" is string`)
    } else {
        console.log(`Type of value "${data}" is indeterminate`)
    }
};
checkType(7);