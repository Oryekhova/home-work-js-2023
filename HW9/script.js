//Task 1
console.log("==Creation of class and object==");
class Person {
    constructor(name, age){
        this._name = name;
        this._age = age;
    }
    sayHello(){
        console.log(`Hello ${this._name}!`)
    }
}

let user = new Person("John", 30);
console.log(user);

//Task 2
console.log("==Methods of class==");

user.sayHello();

//Task 3
console.log("==Inheritance==")
class Student extends Person {
    constructor(name, age, studentId){
        super (name, age);
        this._studentId = studentId;
    }
    study(){
        console.log(`Student ${this._name} has ${this._age} years old. He studies: ${this._studentId}`)
    }
}

let student = new Student ("Leo", 19, "Java Script");
console.log(student);
student.study();