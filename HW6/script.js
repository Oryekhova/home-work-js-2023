
//Task 1 (Calculator)
let mathOperat = prompt('Enter the sign of mathematical operation (+ - * /):');
let firstNumber = Number(prompt('Enter a first number:'));
let secondNumber = Number(prompt('Enter a second number:'));
switch (mathOperat) {
    case "+":
        console.log(`${firstNumber}+${secondNumber}=`, firstNumber+secondNumber)
        break;
    case "-":
        console.log(`${firstNumber}-${secondNumber}=`, firstNumber-secondNumber)
        break;
    case "*":
        console.log(`${firstNumber}*${secondNumber}=`, firstNumber*secondNumber)
        break;
    case "/":
        console.log(`${firstNumber}/${secondNumber}=`, firstNumber/secondNumber)
        if (secondNumber===0){
            alert("Division by 0 is not possible");
        } 
        break;

    default:
        console.log("You entered a wrong sign");
        break;
}

//Task 2
let arrNames = ["Leo", "Nick", "Jack"];

function wellcomUser(arr, funcCall){
for(let i=0; i<arr.length; i++){
    funcCall(arr[i])
}
}
function hiUser(name){
    console.log(`Hello ${name}!`)}
wellcomUser(arrNames, hiUser);
/*
//Task 3
let arrNames = ["Leo", "Nick", "Jack"];
let hiUser = (name)=> console.log(`Hello ${name}!`);
let wellcomUser = (arr, funcCall)=>{for(let i=0; i<arr.length; i++){
        funcCall(arr[i])
    }
}
wellcomUser(arrNames, hiUser);
*/

//Task 4
function sayHi(defaultValue = 'Oksana'){
    console.log(`Hello ${defaultValue}!`);
}
sayHi()
//Task 5
let string = "Hello world!"
function callBack (arg) {
    console.log(`Say "${arg}"`)
}

function main (value, anyFunc) {
    anyFunc(value)
}

main(string, callBack);

//Task 6
function multiplyValues(a, b, c) {
    return a*b*c
}