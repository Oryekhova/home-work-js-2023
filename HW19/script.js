window.onload = function(){
    //Task 1
    let textStr = "    Lorem ipsum dolor sit amet consectetur adipisicing elit. Eos itaque vero laborum fugit harum! Quia, cumque necessitatibus nemo veniam odit dolor impedit aspernatur, ipsa vitae minima quasi doloribus repellendus cupiditate!";
    let bigLetters = textStr.match(/[A-Z]/g);
    console.log(bigLetters);

    //Task 2
    let operation = "5 плюс 7 = 3";
    let numbers = operation.match(/\d/g);
    console.log(numbers);

    //Task 3
    let word = textStr.match(/[a-zA-Z]{5}/gi);
    console.log(word);
}