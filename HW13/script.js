window.onload = function() {
//Task 1
let ulList = document.querySelector('#tree');
let firstLevel = ulList.children;
let quantityFirstLevel = ulList.children.length;
console.log(firstLevel);
console.log(`Number of first level elements: ${quantityFirstLevel}`);

for (let element of firstLevel){
    console.log(element.children);
    console.log(`Number of second level elements: ${element.childElementCount}`);
}

//Task 2
const booksArray = [
    {
        title: 'Alice in Wanderland',
        year: 1865,
        rating: 4.5
    },

    {
        title: '1984',
        year: 1949,
        rating: 4.8
    },

    {
        title: 'Harry Potter',
        year: 1997,
        rating: 4.7
    }
];
let table = document.querySelector('thead');

for(let list of booksArray){
    let newRow = document.createElement('tr');
    table.after(newRow);
    let newCell = document.createElement('td');
    newRow.appendChild(newCell);
    newCell.innerHTML = list.title;
    let newCell1 = document.createElement('td');
    newRow.appendChild(newCell1);
    newCell1.innerHTML = list.year;
    let newCell2 = document.createElement('td');
    newRow.appendChild(newCell2);
    newCell2.innerHTML = list.rating;
}

//Task 3
const elementsArray = [
    { tag: 'p', text: 'Елемент 1' },
    { tag: 'div', text: 'Елемент 2' },
    { tag: 'span', text: 'Елемент 3' }
];
let container = document.querySelector('.elements');
for (let element of elementsArray){
newTag = document.createElement(element.tag);
container.appendChild(newTag);
newTag.innerHTML = element.text;
}

//Task 4
const addArray = [
    { text: 'Елемент 1', usePrepend: true },
    { text: 'Елемент 2', usePrepend: false },
    { text: 'Елемент 3', usePrepend: true }
]
let list = document.querySelector('.prepend');
for (let el of addArray){
    paragraph = document.createElement('p');
    if (el.usePrepend) {
    list.prepend(paragraph)}
    else  {
        list.after(paragraph)    
    }
    paragraph.innerHTML = el.text;
    }
}
