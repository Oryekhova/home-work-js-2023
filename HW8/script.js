/*
//Task 1
for (let i = 0; i < 5; i++) {
    console.log(i);
}
for (const i=0; i < 5; i++) {
    console.log(i);
   
}*/
// Замість var

let message = 'test';

function example() {
    if (true) {
        let message = 'Hello, world!';
        console.log(message);
    }
    console.log(message); // Виведе 'Hello, world!'
}
example();

//Task 2
console.log("-----Task 2-----");
const person = {
    name: 'John',
    age: 25,
    occupation: 'Developer'
};

for (const key in person) {
    console.log(key, person[key])}

//Task 3
console.log("-----Task 3-----");
const students = [
    { name: 'Alice', age: 20, grade: 'A' },
    { name: 'Bob', age: 22, grade: 'B' },
    { name: 'Charlie', age: 21, grade: 'C' }
];
for (const value of students) {
    console.log(`User info: ${value.name} has age: ${value.age} and grade: ${value.grade}`);
}

for (const value in students) {
    console.log(value, students[value])}

