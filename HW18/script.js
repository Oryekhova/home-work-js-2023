window.onload = function(){
//Task 1
let page = document.querySelector('body');
let formEl = document.querySelector('#changebg');
let [...radioButtons] = document.querySelectorAll('input[name="color"]');
    formEl.addEventListener('change', ()=>{
        for(el of radioButtons){
            if(el.checked){
                page.style.background = el.value}
                 };
    });

//Task 2
let formList = document.querySelector('.formhobby');
let res = document.querySelector('.resultBox');
let [...checkboxes] = document.querySelectorAll('input[name="Hobbies"]');
formList.addEventListener('change', ()=>{
    res.innerHTML ="";
    checkboxes.forEach(item=>{
        if(item.checked){
        let pEl = document.createElement('p');
        pEl.innerText = item.value;
        res.appendChild(pEl);  };
    });
});

//Task 3
let selectEl = document.querySelector('#country');
let capital = document.querySelector('.result');
selectEl.addEventListener('change', function(){
capital.innerText = this.value;});

//Task 4
let formRate = document.querySelector('#rating');
let [...allMarks] = document.querySelectorAll('input[name="rate"]');
let resMark = document.querySelector('.mark');
formRate.addEventListener('change', function(){
    for(element of allMarks){
        if(element.checked){
            resMark.innerText = `Thank you for your mark: ${element.value}`;}
             };
    });
}
