window.onload = function(){
//1. OnMouseOver and onMouseOut
let square = document.querySelector('.block');
square.addEventListener('mouseover', function(e){
    this.style.background = 'lightblue';
});
square.addEventListener('mouseout', function(e){
    this.style.background = '';
});

//2. OnContextMenu
let text = document.querySelector('p');
let menu = document.querySelector('.menu');
let body = document.querySelector('body');
text.addEventListener('contextmenu', function(e){
    e.preventDefault();
    menu.style.display = 'block';
    menu.style.left = e.clientX + 'px';
    menu.style.top = e.clientY + 'px';
});
body.addEventListener('click', function(){
    menu.style.display = 'none';
});

//3. OnMouseOver and onMouseOut
let city = document.querySelector('img');
city.addEventListener('mouseover', function(e){
    this.classList.toggle('foto');
});
city.addEventListener('mouseout', function(e){
    this.classList.toggle('foto');
});

//4. Keyup
/*let ball = document.querySelector('.ball');
let box = document.querySelector('.box');
let left = 0;
let top = 0;
const boxRect = box.getBoundingClientRect();
const ballRect = ball.getBoundingClientRect();
const maxLeft = boxRect.width - ballRect.width;
const maxTop = boxRect.height - ballRect.height;
body.addEventListener('keydown', function(e){
   e.preventDefault();
   if(e.key == 'ArrowRight' & left < maxLeft){
    ball.style.left = left + 'px';
    left++};
   if(e.key == 'ArrowLeft' & left > 0){
    ball.style.left = left + 'px';
    left--};
   if(e.key == 'ArrowDown' & top < maxTop){
    ball.style.top = top + 'px';
    top++};
   if(e.key == 'ArrowUp' & top > 0){
    ball.style.top = top + 'px';
    top--};
 }); */

 //'keydown' and 'keyup'
 let contextList = document.querySelector('.context');
 let [...listLi] = document.querySelectorAll('.context li');
 let nextEl = contextList.nextElementSibling;
 document.addEventListener('keydown', function(e){
   if(e.code == 'KeyM'){
    contextList.style.display = 'block'}
 });

  
        document.addEventListener('keyup', function(e){
                if (contextList.style.display === 'block' & e.key == 'ArrowDown' ){
                nextEl.style.background = 'darkblue';
                nextEl.style.color = 'white';
            };
        });
        
        


    document.addEventListener('keydown', function(e){
        if(e.code == 'KeyC'){
         contextList.style.display = 'none'}
      });

}