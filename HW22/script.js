//Task 1
let number = 5;
let myPromise = new Promise(resolve=>resolve(number))
.then(value=>value*4)
.then(value=>value-10)
.then(value=>console.log(`Result of first Task: ${value}`)).catch((error)=>{
    console.log(`Promise Error: ${error}`);
});

//Task 2

let myString = 'Make it Uppercase';

let newString = text => {
    return new Promise(resolve => {
        setTimeout(function(){
            console.log('Result of second Task:', text.toUpperCase());
            resolve();
        }, 2000);
});}

newString(myString).then(function(value){
    console.log(value);
}).catch(function(error){
   console.log('Promise error:', error);
});

//Task 3

let firstNumber = 2;
let secondNumber = 2;

let compare = function(a,b){
    return new Promise (function(resolve, reject){
if (a>b){
    resolve(`Result of third Task: ${a}>${b}`);
}
if (b>a){
    resolve(`Result of third Task: ${b}>${a}`)
}
else {
    reject (`Result of third Task "The numbers are equal"`)
}
    })
};

compare(firstNumber, secondNumber).then(function(value){
    console.log(value);
}).catch(function(error){
    console.log(`Promise catch error: ${error}`);
});

