window.onload = function()  {
    let bodyEl = document.querySelector('body');
    let headerEl = document.createElement('header');
    bodyEl.appendChild(headerEl);
    headerEl.setAttribute('style', 'padding:50px');
    headerEl.style.backgroundColor ='yellow';
    headerEl.style.textAlign ='center';
    const menuData = [
        { name: 'Main   ', url: '/' },
        { name: 'About us   ', url: '/about' },
        { name: 'Services   ', url: '/services' },
    ];
    for (let menu of menuData){
        let link = document.createElement('a');
        headerEl.appendChild(link);
        link.innerHTML = menu.name;
        link.setAttribute('href', menu.url);
        link.setAttribute('target', '_blank');
    }
    let container = document.createElement('container');
    headerEl.after(container);
    container.style.display ='flex';
    for (let i = 0; i<=50; i++){
    let div = document.createElement('div');
    container.appendChild(div);
    div.style.borderRadius ='50%';
    div.style.width ='50px';
    div.style.height ='50px';
    div.style.backgroundColor ='gray';
    div.setAttribute('class', 'circle-elemt');}
    
}