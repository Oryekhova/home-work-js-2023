window.onload = function(){
    //Task 1
    let message = "Hello!"
    function showMessage (text){
        console.log(text);
    };

    setTimeout(showMessage, 3000, message);

    //Task 2
    let btn = document.querySelector('.btnTimer');
    let counter = 0;
    btn.addEventListener('click', function(){
        function count(){
            counter++;
            document.querySelector('p').innerHTML = counter;
            if(counter>=10){
                clearInterval(interval);
            }
        }
        let interval = setInterval(count, 1000);
    });
    //Task 3
    let changeColor = document.querySelector('#color');
    function getRandomColor(){
        const r = Math.floor(Math.random()*256);
        const g = Math.floor(Math.random()*256);
        const b = Math.floor(Math.random()*256);
        return `rgb(${r},${g},${b})`;
    }
    changeColor.addEventListener('click', function(){
        let root = document.documentElement;
        root.style.setProperty('--bg-color', getRandomColor())
    })
    
    //Timer
    let seconds = document.querySelector('#sec');
    let mlSeconds = document.querySelector('#mlsec');
    let startBtn = document.querySelector('.start');
    let resetBtn = document.querySelector('.reset');
    let timeSec = 25;
    let timeMlsec = 100;
    
    startBtn.addEventListener('click', function(){
        startBtn.disabled = true;
        function timerSec(){
            timeSec--;
            seconds.innerHTML = timeSec;
            if (timeSec<10){
                seconds.innerHTML = '0'+timeSec;
            };
            if(timeSec<=0){
                clearInterval(endSec);
                clearInterval(endMlsec);
                mlSeconds.innerHTML = '00';
                seconds.innerHTML = '00';
            }
        }
        let endSec = setInterval(timerSec, 1000);
    
        function timerMlsec(){
            mlSeconds.innerHTML = timeMlsec;
            timeMlsec--;
            if (timeMlsec<10){
                mlSeconds.innerHTML = '0'+timeMlsec;
            };
            if (timeMlsec<0){
                timeMlsec=99;
            }
        }
        let endMlsec = setInterval(timerMlsec, 10);
        resetBtn.addEventListener('click', function(){
            startBtn.disabled = false; 
            timeSec = 25;
            timeMlsec = 100;
            seconds.innerHTML = '25';
            mlSeconds.innerHTML = '00';
            clearInterval(endSec);
            clearInterval(endMlsec); 
            });  
    });
     
}