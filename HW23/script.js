window.onload = function(){
    //Task 1
    console.log('Task 1:')
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(data => {data.forEach(element => {
        console.log(`User's name: ${element.name}; User's e-mail: ${element.email}`)
      });});

      //Task 2
      let userInfo = document.querySelector('#userInfo');
      fetch('https://jsonplaceholder.typicode.com/users/1')
      .then(response => response.json())
      .then(json => userInfo.innerHTML = `Information about 1 user: <br><h3>Name:</h3> ${json.name} <br><h3>Username:</h3> ${json.username} <br><h3>E-mail:</h3> ${json.email}`);

      //Task 3
      let userPosts = document.querySelector('#userPosts');
      fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
      .then(response => response.json())
      .then(data => {data.forEach(element => {
        let divEl = document.createElement('div');
        divEl.innerHTML = element.title;
        userPosts.appendChild(divEl);
      });});

      //Task 4
      let btn = document.querySelector('#btnSW');
    let infoBlock = document.querySelector('#about');

    btn.addEventListener('click', function(){
        infoBlock.style.display = 'block';
        fetch('https://swapi.dev/api/people/')
      .then(response => response.json())
      .then(data => {data.results.forEach(element => {
        let divEl = document.createElement('div');
          
          divEl.innerHTML = `<br>Name: ${element.name}
          <br>Height: ${element.height}
          <br>Mass: ${element.mass}
          <br>Hair color: ${element.hair_color}`;
          infoBlock.appendChild(divEl);
         
        
          let linkEl = document.createElement('a');
          linkEl.setAttribute('href', element.films);
          linkEl.innerHTML = `<br>${element.films}`;
          divEl.appendChild(linkEl);
      });});
    });
}