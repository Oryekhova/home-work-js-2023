let result1 = (5 + 3) * 2 - 7;

let result2 = (10 / 2) ** 2;

let result3 = 2 * (4 + 3) - (8 / 2);

let result4 = (15 % 4) + 5;

let result5 = 3 - (2 ** 4);

console.log("result1: " + result1);
console.log("result2: " + result2);
console.log("result3: " + result3);
console.log("result4: " + result4);
console.log("result5: " + result5);

let firstNumber = 15;
let secondNumber = 5;

console.log(`${firstNumber+secondNumber}`);
console.log(`${firstNumber-secondNumber}`);
console.log(`${firstNumber*secondNumber}`);
console.log(`${firstNumber/secondNumber}`);
console.log(`${firstNumber%secondNumber}`);
console.log(`${firstNumber**2}`);