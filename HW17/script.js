window.onload = function(){
//Task 1. Currency Converter
let form = document.querySelector('.convertForm');
let button = document.querySelector('#mybtn');
let quantity = document.querySelector('#summ');
let exchange = document.querySelector('#change');
let result = document.querySelector('.result');

button.addEventListener('click', function(e){
    e.preventDefault();
    result.innerHTML = `You'll become ${quantity.value*exchange.value} UAH`;
    

    if(quantity.value === '' || exchange.value === ''){
    alert('Enter a value')
}
form.reset();})

//Task 2. Dynamic task list
let formList = document.querySelector('.taskList');
let addBtn = document.querySelector('#addbtn');
let text = document.querySelector('#task');
let newList = document.querySelector('.list');

addBtn.addEventListener('click', function(e){
    e.preventDefault();
    if(text.value === ''){
        alert('Enter a text')}
        else {
    let newLi = document.createElement('li');
    let removeBtn = document.createElement('button');
    removeBtn.innerHTML = 'Delete';
    newList.appendChild(newLi);
    newLi.innerHTML = text.value;
    newLi.appendChild(removeBtn);
    removeBtn.classList.add('mybutton');

    formList.reset();
    removeBtn.addEventListener('click', function(){
        newLi.remove();
    })}
})
}